import server

def test_addition():
    assert server.addition(2, 3) == 5

def test_subtraction():
    assert server.subtraction(9, 5) == 4

def test_multiplication():
    assert server.multiplication(2, 2) == 4
